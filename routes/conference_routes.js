var express = require('express');
var router = express.Router();
var conference_dal = require('../model/conference_dal');
var team_dal = require('../model/team_dal');


// View All companys
router.get('/all', function(req, res) {
    conference_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('conference/conferenceViewAll', { 'result':result });
        }
    });

});


// View the company for the given id
router.get('/', function(req, res){
    if(req.query.conference_id == null) {
        res.send('conference_id is null');
    }
    else {
        conference_dal.getById(req.query.conference_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('conference/conferenceViewById', {'result': result});
            }
        });
    }
});




module.exports = router;
