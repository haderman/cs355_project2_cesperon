var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');


// View All companys
// View the company for the given id
router.get('all', function(req, res){
    if(req.query.conference_id == null) {
        res.send('conference_id is null');
    }
    else {
        team_dal.getAll(req.query.conference_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('team/teamViewAll', {'result': result});
            }
        });
    }
});


// View the company for the given id
router.get('/', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.getById(req.query.team_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('team/teamViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

            res.render('team/teamAdd');

});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.team_name == null) {
        res.send('team must be provided.');
    }
    else if(req.query.coach == null) {
        res.send('At least one address must be selected');
    }
    else if(req.query.city == null) {
        res.send('Must provide a city')
    }
    else if(req.query.state == null) {
        res.send('Must provide a state')
    }
    else if(req.query.conference_id == null){
        res.send('Must provide conference id')
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        team_dal.insert(req.query, function(err,result) {

                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/conference/all');
            });
    }
});

router.get('/stats', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    team_dal.edit(function(err,result){
        res.render('team/teamStats',{'result':result});
    });


});








module.exports = router;
