var express = require('express');
var router = express.Router();
var player_dal = require('../model/player_dal');
var season_dal = require('../model/season_dal');


// View All companys
router.get('/all', function(req, res) {
    player_dal.getAll(function(err, player){
        season_dal.getAll(function(err, season) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerGetAll', {'player': player, 'season':season});
            }
        })
    });

});




// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.player_id == null) {
        res.send('player must be selected.');
    }
    else if(req.query.season_id == null) {
        res.send('At least one season must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        player_dal.insert(req.query.player_id, req.query.season_id, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.render('player/playerGetStats', {'result':result});
            }
        });
    }
});

router.get('/points', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    player_dal.edit(function(err,result){
        res.render('player/playerPointLeaders',{'result':result[0]});
    });


});


module.exports = router;